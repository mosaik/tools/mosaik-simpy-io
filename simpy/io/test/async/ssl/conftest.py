import os

import pytest

from simpy.io import async
from simpy.io import test


testdir = os.path.dirname(test.__file__)


class DefaultSSLSocket(async.SSLSocket):
    def __init__(self, env, sock=None, **kwargs):
        kwargs['keyfile'] = os.path.join(testdir, 'server.key')
        kwargs['certfile'] = os.path.join(testdir, 'cacert.pem')
        async.SSLSocket.__init__(self, env, sock, **kwargs)


@pytest.fixture()
def env(request):
    env = async.Environment()
    request.addfinalizer(env.close)
    return env


@pytest.fixture()
def link_type(env, request):
    return DefaultSSLSocket

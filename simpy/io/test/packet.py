from simpy.io.packet import Packet, Header


def test_packet(env, link_type):
    server = link_type.server(env, ('127.0.0.1', 0))
    conn = link_type.connection(env, server.address)
    peer_a = Packet((yield server.accept()))
    peer_b = Packet((yield conn))

    yield peer_a.write(b'hello, what is your name?')
    data = yield peer_b.read()
    assert data == b'hello, what is your name?'

    yield peer_b.write(b'bob')
    data = yield peer_a.read()
    yield peer_a.write(b'nice to meet you ' + data)

    data = yield peer_b.read()
    assert data == b'nice to meet you bob'


def test_packet_size_check(env, link_type):
    server = link_type.server(env, ('127.0.0.1', 0))
    conn = link_type.connection(env, server.address)
    peer_a = Packet((yield server.accept()), max_packet_size=1)
    peer_b = Packet((yield conn))

    try:
        yield peer_a.write(b'spam')
        assert False, 'Expected an exception'
    except ValueError as e:
        # TODO Check traceback for readability.
        assert e.args[0] == 'Packet too large. Allowed 1 bytes but got 4 bytes'


def test_packet_chunks(env, link_type):
    server = link_type.server(env, ('127.0.0.1', 0))
    conn = link_type.connection(env, server.address)
    peer_a = Packet((yield server.accept()))
    peer_b = yield conn

    send_payloads = [b'eggs', b'spam']
    for payload in send_payloads:
        packet = Header.pack(len(payload)) + payload
        for i in range(len(packet)):
            yield peer_b.write(packet[i:i+1])

    recv_payloads = [(yield peer_a.read()), (yield peer_a.read())]

    assert recv_payloads == send_payloads
